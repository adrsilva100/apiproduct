package br.com.product.model.converter;

import javax.ws.rs.NotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.product.config.ProductApplication;
import br.com.product.model.conversor.ImageConverter;
import br.com.product.model.dto.ImageDTO;
import br.com.product.model.dto.ProductDTO;
import br.com.product.persistence.entity.Image;
import br.com.product.persistence.entity.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProductApplication.class })
@WebAppConfiguration
public class ImageConverterTest {

	@Autowired
	private ImageConverter imageConverter;
	private Image image;

	@Before
	public void before(){
		Product product = new Product();
		product.setName("Product A");
		product.setDescription("cleaning");

		image = new Image();
		image.setId(1L);
		image.setType("jpeg");
		image.setProduct(product);
	}	

	@Test
	public void convertFromImageDTO(){
		ImageDTO imageDTO = imageConverter.convertFromImageDTO(image);

		Assert.assertEquals("jpeg", imageDTO.getType());
		Assert.assertNull(imageDTO.getProduct());
	}

	@Test
	public void convertFromImage(){
		ImageDTO imageDTO = new ImageDTO(1L, "gif");
		ProductDTO productDTO = new ProductDTO();
		productDTO.setId(2L);
		productDTO.setName("Product A");
		productDTO.setDescription("automotive");

		imageDTO.setProduct(productDTO);
		Image image = imageConverter.convertFromImage(imageDTO);

		Assert.assertTrue(image.getId().equals(1L));
		Assert.assertEquals("gif", image.getType());
		Assert.assertTrue(image.getProduct().getId().equals(2L));
		Assert.assertEquals("Product A", image.getProduct().getName());
		Assert.assertEquals("automotive", image.getProduct().getDescription());
	}
	
	@Test(expected=NotFoundException.class)
	public void convertFromImageDTOWithImageNull(){
		imageConverter.convertFromImageDTO(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void convertFromImageWithProductNull(){
		ImageDTO imageDTO = new ImageDTO(1L, "gif");
		imageConverter.convertFromImage(imageDTO);
	}
}
