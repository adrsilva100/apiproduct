package br.com.product.model.converter;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.NotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.product.config.ProductApplication;
import br.com.product.model.conversor.ProductConverter;
import br.com.product.model.dto.ProductDTO;
import br.com.product.persistence.entity.Image;
import br.com.product.persistence.entity.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProductApplication.class })
@WebAppConfiguration
public class ProductConverterTest {

	@Autowired
	private ProductConverter productConverter;
	private Product product;

	@Before
	public void before(){
		product = new Product();
		product.setName("Product A");
		product.setDescription("cleaning");
		
		Image image = new Image();
		image.setId(1L);
		image.setType("jpeg");
		image.setProduct(product);
		
		Set<Image> images = new HashSet<Image>();
		images.add(image);
		
		product.setImages(images);
	}	
	
	@Test
	public void converFromProductDTO(){
		ProductDTO productDTO = productConverter.convertFromProductDTO(product);
		
		Assert.assertEquals("Product A", productDTO.getName());
		Assert.assertEquals("cleaning", productDTO.getDescription());
		Assert.assertEquals(1, productDTO.getImages().size());
	}
	
	@Test(expected=NotFoundException.class)
	public void converFromProductDTOWithProductNull(){
		productConverter.convertFromProductDTO(null);
	}
	
	@Test
	public void convertFromWithoutRelationshipProductDTO(){
		ProductDTO productDTO = productConverter.convertFromWithoutRelationshipProductDTO(product);
		
		Assert.assertEquals("Product A", productDTO.getName());
		Assert.assertEquals("cleaning", productDTO.getDescription());
		Assert.assertNull(productDTO.getImages());
	}
}
