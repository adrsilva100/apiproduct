package br.com.product.service;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.product.config.ProductApplication;
import br.com.product.persistence.entity.Product;
import br.com.product.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProductApplication.class })
@WebAppConfiguration
@Transactional
public class ProductServiceTest extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	private ProductService productService;
	private Product product;
	
	@Before
	public void before(){
		product = new Product();
		product.setName("Product A");
		product.setDescription("cleaning");
	}	

	@Test
	public void save() {
		productService.saveOrUpdate(product);
	}
	
	@Test
	public void update() {
		productService.saveOrUpdate(product);
		
		Product productSaved =  productService.findById(1L);
		productSaved.setName("Product B");
		productSaved.setDescription("automotive");
		
		productService.saveOrUpdate(productSaved);
		Product productUpdated =  productService.findById(1L);
		
		Assert.assertEquals("Product B", productUpdated.getName());
	}
}
