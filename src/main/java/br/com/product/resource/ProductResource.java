package br.com.product.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.constants.RestfulUrlPath;
import br.com.product.facade.ProductFacade;
import br.com.product.model.dto.ProductDTO;

@Component
@Path(RestfulUrlPath.URL_PATH)
public class ProductResource {

	@Autowired
	private ProductFacade productFacade;

	@GET
	@Path("/{productId}/parent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findParentByProduct(@PathParam(value="productId") Long productId){
		try{
			return Response.ok(productFacade.findParentByProduct(productId)).build();

		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();

		}
	}

	@POST
	@Path("/{productId}/parent")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addProductsParentByProduct(@PathParam(value="productId") Long productId, List<ProductDTO> productsParent){
		try{
			productFacade.addProductsParentByProduct(productId, productsParent);

		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}

		return Response.ok("Products parents saved with success").build();
	}

	@GET
	@Path("/relationship/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByIdWithRelationShip(@PathParam(value="id") Long id){
		try{
			return Response.ok(productFacade.findByIdWithRelationship(id)).build();

		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();

		}
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByIdWithoutRelationship(@PathParam(value="id") Long id){
		try{
			return Response.ok(productFacade.findByIdWithoutRelationship(id)).build();

		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();

		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAllWithoutRelationShip(){
		try{
			return Response.ok(productFacade.findAllWithoutRelationship()).build();

		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();

		}
	}

	@GET
	@Path("/relationship")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAllWithtRelationShip(){
		try{
			return Response.ok(productFacade.findAllWithtRelationship()).build();

		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();

		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveProduct(ProductDTO productDTO){
		try {
			productFacade.save(productDTO);

		} catch (IllegalArgumentException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();

		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}

		return Response.ok("Product saved with success").build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProduct(ProductDTO productDTO){
		try {
			productFacade.update(productDTO);

		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok("Product updated with success").build();
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProduct(@PathParam(value = "id") Long id){
		try {
			productFacade.remove(id);

		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok("Product deleted with success").build();
	}
}
