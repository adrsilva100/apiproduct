package br.com.product.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.constants.RestfulUrlPath;
import br.com.product.facade.ImageFacade;
import br.com.product.model.dto.ImageDTO;

@Component
@Path(RestfulUrlPath.URL_PATH)
public class ImageResource {
	
	@Autowired
	private ImageFacade imageFacade;
	
	@GET
	@Path("/{productId}/image")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByIdWithRelationShip(@PathParam(value="productId") Long productId){
		try{
			return Response.ok(imageFacade.findByProductId(productId)).build();
			
		}catch (NotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
			
		}
	}
	
	@POST
	@Path("/{productId}/image")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addImagesByProduct(@PathParam(value="productId") Long productId, List<ImageDTO> images){
		try{
			imageFacade.addImagesByProduct(productId, images);
			
		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		
		return Response.ok("Images saved with success").build();
	}
	
	@POST
	@Path("/image")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveProduct(ImageDTO imageDTO){
		try {
			imageFacade.save(imageDTO);
			
		} catch (IllegalArgumentException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
			
		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		
		return Response.ok("Image saved with success").build();
	}
	
	@PUT
	@Path("/image")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProduct(ImageDTO imageDTO){
		try {
			imageFacade.update(imageDTO);
			
		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok("Image updated with success").build();
	}
	
	@DELETE
	@Path("/image/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProduct(@PathParam(value = "id") Long id){
		try {
			imageFacade.remove(id);
			
		}catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.ok("Image deleted with success").build();
	}
}
