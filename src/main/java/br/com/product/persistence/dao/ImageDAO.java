package br.com.product.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.product.persistence.entity.Image;

@Repository
public interface ImageDAO extends JpaRepository<Image, Long>{
	
	@Query("select i from Image i where i.product.id = :productId ")
	public List<Image> findByProductId(@Param("productId") Long productId);

}
