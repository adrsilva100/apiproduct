package br.com.product.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.product.persistence.entity.Product;

@Repository
public interface ProductDAO extends JpaRepository<Product, Long>{
	
	@Query("select p from Product p where p.productParent.id = :productId ")
	public List<Product> findParentByProduct(@Param("productId") Long productId);

}
