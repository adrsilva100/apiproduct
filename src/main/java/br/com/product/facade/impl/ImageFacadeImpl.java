package br.com.product.facade.impl;

import java.util.List;

import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.facade.ImageFacade;
import br.com.product.model.conversor.ImageConverter;
import br.com.product.model.dto.ImageDTO;
import br.com.product.persistence.entity.Image;
import br.com.product.service.ImageService;

@Component
public class ImageFacadeImpl implements ImageFacade{
	
	@Autowired
	private ImageConverter imageConverter;
	
	@Autowired
	private ImageService imageService;

	@Override
	public void save(ImageDTO imageDTO) {
		if(imageDTO != null && imageDTO.getId() != null){
			throw new IllegalArgumentException("Field id is not null");
		}
		imageService.saveOrUpdate(imageConverter.convertFromImage(imageDTO));
	}

	@Override
	public void remove(Long id) {
		imageService.remove(id);
	}
	
	@Override
	public void update(ImageDTO imageDTO) {
		if(imageDTO != null && imageDTO.getId() == null){
			throw new IllegalArgumentException("Field id is null");
		}else{
			if(imageService.findById(imageDTO.getId()) == null){
				throw new NotFoundException("Image not found");
			}
		}
		imageService.saveOrUpdate(imageConverter.convertFromImage(imageDTO));
	}

	@Override
	public List<ImageDTO> findByProductId(Long productId) {
		return imageConverter.convertFromListImagesDTO(imageService.findByProduct(productId));
	}

	@Override
	public void addImagesByProduct(Long productId, List<ImageDTO> imagesDTO) {
		List<Image> images = imageConverter.convertFromListImage(productId, imagesDTO);
		
		images.forEach(item->{
			imageService.saveOrUpdate(item);
		});
	}
}