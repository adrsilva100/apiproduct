package br.com.product.facade.impl;

import java.util.List;

import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.facade.ProductFacade;
import br.com.product.model.conversor.ProductConverter;
import br.com.product.model.dto.ProductDTO;
import br.com.product.persistence.entity.Product;
import br.com.product.service.ProductService;

@Component
public class ProductFacadeImpl implements ProductFacade{
	
	@Autowired
	private ProductConverter productConverter;
	
	@Autowired
	private ProductService productService;

	@Override
	public void save(ProductDTO productDTO) {
		if(productDTO != null && productDTO.getId() != null){
			throw new IllegalArgumentException("Field id is not null");
		}
		productService.saveOrUpdate(productConverter.convertFromProduct(productDTO));
	}

	@Override
	public ProductDTO findByIdWithRelationship(Long id) {
		return productConverter.convertFromProductDTO(productService.findById(id));
	}
	
	@Override
	public ProductDTO findByIdWithoutRelationship(Long id) {
		return productConverter.convertFromWithoutRelationshipProductDTO(productService.findById(id));
	}

	@Override
	public void remove(Long id) {
		productService.remove(id);
	}
	
	@Override
	public void update(ProductDTO productDTO) {
		if(productDTO != null && productDTO.getId() == null){
			throw new IllegalArgumentException("Field id is null");
		}else{
			if(productService.findById(productDTO.getId()) == null){
				throw new NotFoundException("Product not found");
			}
		}
		productService.saveOrUpdate(productConverter.convertFromProduct(productDTO));
	}

	@Override
	public List<ProductDTO> findAllWithoutRelationship() {
		return productConverter.convertFromListProducsDTOtWithoutRelationShip(productService.findAll());
	}

	@Override
	public List<ProductDTO> findAllWithtRelationship() {
		return productConverter.convertFromListProducsDTOtWithRelationShip(productService.findAll());
	}

	@Override
	public List<ProductDTO> findParentByProduct(Long productId) {
		return productConverter.convertFromListProducsDTOtWithoutRelationShip(productService.findByProduct(productId));
	}

	@Override
	public void addProductsParentByProduct(Long productId, List<ProductDTO> productsParent) {
		List<Product> parents = productConverter.convertFromListProduct(productId, productsParent);
		
		parents.forEach(item->{
			productService.saveOrUpdate(item);
		});
		
	}
}