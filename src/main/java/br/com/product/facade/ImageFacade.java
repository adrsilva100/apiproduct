package br.com.product.facade;

import java.util.List;

import br.com.product.model.dto.ImageDTO;

public interface ImageFacade {
	
	public void save(ImageDTO imageDTO);
	public void remove(Long id);
	public void update(ImageDTO imageDTO);
	public List<ImageDTO> findByProductId(Long id);
	public void addImagesByProduct(Long productId, List<ImageDTO> imagesDTO);
}
