package br.com.product.facade;

import java.util.List;

import br.com.product.model.dto.ProductDTO;

public interface ProductFacade {
	
	public void save(ProductDTO productDTO);
	public void remove(Long id);
	public void update(ProductDTO productDTO);
	public List<ProductDTO> findAllWithoutRelationship();
	public List<ProductDTO> findAllWithtRelationship();
	public ProductDTO findByIdWithoutRelationship(Long id);
	public ProductDTO findByIdWithRelationship(Long id);
	public List<ProductDTO> findParentByProduct(Long productId);
	public void addProductsParentByProduct(Long productId, List<ProductDTO> productsParent);
}
