package br.com.product.model.dto;

import java.io.Serializable;

public class ImageDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;
	private String type;
	private ProductDTO product;
	
	public ImageDTO() {}
	
	public ImageDTO(Long id, String type) {
		super();
		this.id = id;
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ProductDTO getProduct() {
		return product;
	}
	public void setProduct(ProductDTO product) {
		this.product = product;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
