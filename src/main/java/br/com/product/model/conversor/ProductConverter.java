package br.com.product.model.conversor;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.model.dto.ImageDTO;
import br.com.product.model.dto.ProductDTO;
import br.com.product.persistence.entity.Product;
import br.com.product.service.ProductService;

@Component
public class ProductConverter {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ImageConverter imageConverter;
	
	public Product convertFromProduct(ProductDTO productDTO) {
		Product product = new Product();
		validate(productDTO);
		
		product.setId(productDTO.getId());
		product.setName(productDTO.getName());
		product.setDescription(productDTO.getDescription());
		populateProductParent(productDTO, product);
		
		return product;
	}
	
	public List<ProductDTO> convertFromListProducsDTOtWithoutRelationShip(List<Product> listProducts) {
		if(listProducts.size() == 0){
			throw new NotFoundException("Products not found");
		}

		List<ProductDTO> products = new ArrayList<ProductDTO>();
		if(listProducts != null){
			listProducts.forEach(item->{
				ProductDTO productDTO = new ProductDTO();

				productDTO.setId(item.getId());
				productDTO.setName(item.getName());
				productDTO.setDescription(item.getDescription());
				
				products.add(productDTO);
			});
		}
		
		return products;
	}
	
	public List<ProductDTO> convertFromListProducsDTOtWithRelationShip(List<Product> listProducts) {
		List<ProductDTO> products = new ArrayList<ProductDTO>();
		
		if(listProducts != null){
			listProducts.forEach(item->{
				ProductDTO productDTO = new ProductDTO();

				productDTO.setId(item.getId());
				productDTO.setName(item.getName());
				productDTO.setDescription(item.getDescription());
				populateProductParentDTO(item, productDTO);
				populateImages(item, productDTO);
				
				products.add(productDTO);
			});
		}
		
		return products;
	}
	
	public List<Product> convertFromListProduct(Long productId, List<ProductDTO> productsDTO) {
		List<Product> products = new ArrayList<Product>();
		ProductDTO productParentDTO = new ProductDTO();
		
		Product product = productService.findById(productId);
		if(product != null){
			productParentDTO.setId(product.getId());
		}else{
			throw new NotFoundException("Product father not found");
		}
		
		if(productsDTO != null){
			productsDTO.forEach(item->{
				item.setProductParent(productParentDTO);
				products.add(convertFromProduct(item));
			});
		}
		
		return products;
	}

	private void populateProductParent(ProductDTO productDTO, Product product) {
		if(productDTO.getProductParent() != null){
			Product productParent = productService.findById(productDTO.getProductParent().getId());
			
			if(productParent == null){
				throw new IllegalArgumentException("Field productParent not found");
			}
			product.setProductParent(productParent);
		}
	}
	
	public ProductDTO convertFromProductDTO(Product product) {
		ProductDTO productDTO = new ProductDTO();
		
		if(product == null){
			throw new NotFoundException("Product not found");
		}
		
		productDTO.setId(product.getId());
		productDTO.setName(product.getName());
		productDTO.setDescription(product.getDescription());
		populateImages(product, productDTO);
		populateProductParentDTO(product, productDTO);
		
		return productDTO;
	}
	
	public ProductDTO convertFromWithoutRelationshipProductDTO(Product product) {
		ProductDTO productDTO = new ProductDTO();
		
		if(product == null){
			throw new NotFoundException("Product not found");
		}
		
		productDTO.setId(product.getId());
		productDTO.setName(product.getName());
		productDTO.setDescription(product.getDescription());
		
		return productDTO;
	}

	private void populateProductParentDTO(Product product, ProductDTO productDTO) {
		validateProductParent(productDTO);
		
		if(product.getProductParent() != null){
			ProductDTO producParentDTO = new ProductDTO();
			
			producParentDTO.setId(product.getProductParent().getId());
			producParentDTO.setName(product.getProductParent().getName());
			producParentDTO.setDescription(product.getProductParent().getDescription());
			populateImages(product, productDTO);
			
			productDTO.setProductParent(producParentDTO);
		}
	}

	private void populateImages(Product product, ProductDTO productDTO) {
		List<ImageDTO> images = new ArrayList<ImageDTO>();
		validateImages(productDTO);
		
		if(product.getImages() != null && product.getImages().size() > 0){
			product.getImages().forEach(item->{
				images.add(imageConverter.convertFromImageDTO(item));
			});
			productDTO.setImages(images);
		}
	}
	
	private static void validateProductParent(ProductDTO productDTO){
		if(productDTO.getProductParent() != null && productDTO.getProductParent().getId() == null){
			throw new IllegalArgumentException("Field productParent is null");
		}
	}
	
	private static void validateImages(ProductDTO productDTO){
		if(productDTO.getImages() != null && productDTO.getImages().size() > 0){
			productDTO.getImages().forEach(item->{
				if(item.getId() == null){
					throw new IllegalArgumentException("There are images is null");
				}
			});
		}
	}
	
	private static void validate(ProductDTO productDTO){
		if (productDTO.getName() == null || productDTO.getName() == "") {
			throw new IllegalArgumentException("Field name is null");
		}
	}
}
