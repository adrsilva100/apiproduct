package br.com.product.model.conversor;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.model.dto.ImageDTO;
import br.com.product.model.dto.ProductDTO;
import br.com.product.persistence.entity.Image;
import br.com.product.persistence.entity.Product;
import br.com.product.service.ProductService;

@Component
public class ImageConverter {
	
	@Autowired
	private ProductService productService;
	
	public Image convertFromImage(ImageDTO imageDTO) {
		Image image = new Image();
		
		validate(imageDTO);
		image.setId(imageDTO.getId());
		image.setType(imageDTO.getType());
		Product product = populateProduct(imageDTO);
		image.setProduct(product);
		
		return image;
	}
	
	private static void validate(ImageDTO imageDTO){
		if (imageDTO.getProduct() == null) {
			throw new IllegalArgumentException("Field product is null");
		}
	}

	private Product populateProduct(ImageDTO imageDTO) {
		Product product = new Product();
		
		if(imageDTO.getProduct() != null){
			product.setId(imageDTO.getProduct().getId());
			product.setName(imageDTO.getProduct().getName());
			product.setDescription(imageDTO.getProduct().getDescription());
		}
		
		return product;
	}
	
	public ImageDTO convertFromImageDTO(Image image) {
		if(image == null){
			throw new NotFoundException("Image not found");
		}
		
		ImageDTO imageDTO = new ImageDTO(image.getId(), image.getType());
		return imageDTO;
	}
	
	public List<ImageDTO> convertFromListImagesDTO(List<Image> listImages) {
		if(listImages.size() == 0){
			throw new NotFoundException("Images not found");
		}
		
		
		List<ImageDTO> images = new ArrayList<ImageDTO>();
		if(listImages != null){
			listImages.forEach(item->{
				ImageDTO imageDTO = new ImageDTO(item.getId(), item.getType());

				images.add(imageDTO);
			});
		}
		
		return images;
	}
	
	public List<Image> convertFromListImage(Long productId, List<ImageDTO> imagesDTO) {
		List<Image> images = new ArrayList<Image>();
		ProductDTO productDTO = new ProductDTO();
		
		Product product = productService.findById(productId);
		if(product != null){
			productDTO.setId(product.getId());
		}
		
		if(imagesDTO != null){
			imagesDTO.forEach(item->{
				item.setProduct(productDTO);
				Image image = convertFromImage(item);
				
				images.add(image);
			});
		}
		
		return images;
	}
}
