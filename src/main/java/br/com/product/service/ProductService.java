package br.com.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.persistence.dao.ProductDAO;
import br.com.product.persistence.entity.Product;

@Component
public class ProductService {
	
	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private ImageService imageService;
	
	public void saveOrUpdate(Product product){
		productDAO.save(product);
	}
	
	public Product findById(Long id){
		return productDAO.findOne(id);
	}
	
	public void remove(Long id){
		Product product = findById(id);
		if(product != null && product.getImages().size() > 0){
			product.getImages().forEach(item->{
				imageService.remove(item.getId());
			});
		}
		productDAO.delete(id);
	}
	
	public List<Product> findAll(){
		return productDAO.findAll();
	}
	
	public List<Product> findByProduct(Long productId){
		return productDAO.findParentByProduct(productId);
	}
}
