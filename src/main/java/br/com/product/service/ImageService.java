package br.com.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.product.persistence.dao.ImageDAO;
import br.com.product.persistence.entity.Image;

@Component
public class ImageService {
	
	@Autowired
	private ImageDAO imageDAO;
	
	public void saveOrUpdate(Image image){
		imageDAO.save(image);
	}
	
	public Image findById(Long id){
		return imageDAO.findOne(id);
	}
	
	public void remove(Long id){
		imageDAO.delete(id);
	}
	
	public List<Image> findByProduct(Long productId){
		return imageDAO.findByProductId(productId);
	}
}
