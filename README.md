# README #

For test this application:

- Clone the repository: git clone https://adrsilva100@bitbucket.org/adrsilva100/apiproduct.git
- Run the command in the root folder of the repository:  mvn clean install spring-boot:run

Restfull URLS:

HTTP POST = localhost:8080/api/v1/product/ : Create a product
Input: {"name":"Product A", "description" : "cleaning product"} 
Return: Product saved with success

HTTP GET = localhost:8080/api/v1/product/{idProduct} : Find a specific product
Return: {"name":"Product A", "description" : "cleaning product"} 

HTTP DELETE = localhost:8080/api/v1/product/{idProduct} : Delete a specific product
Return: Product deleted with success

HTTP PUT = localhost:8080/api/v1/product/ : Update a product
Input: {"id":"1","name":"Product B", "description" : "automotive product"}
Return: Product updated with success

HTTP POST = localhost:8080/api/v1/product/image : Create a image
Input: {"type" : "jpeg", "product":{"id":"1"}}
Return: Image saved with success

HTTP PUT = localhost:8080/api/v1/product/image : Update a image
Input: {"id":"1","type" : "gif", "product":{"id":"1"}}
Return: Image updated with success

HTTP DELETE = localhost:8080/api/v1/product/image/{idImage} : Delete a image
Return: Image deleted with success

HTTP GET = localhost:8080/api/v1/product : Find all products without relationship
Return: [
{"id": 1,"name": "Product A", "description": "cleaning",
 "productParent": null,
 "images": null},
          {"id": 2,
"name": "Product B","description": "automotive",
"productParent": null,"images": null
}
]

HTTP GET = localhost:8080/api/v1/product/relationship : Find all products with relationship
Return: [
{"id": 1,"name": "Product A", "description": "cleaning",
 "productParent": null,
 "images": null}
,[
	  
{"id": 2,"name": "Product B", "description": "automotive",
 "productParent": {"id":1,"name":"Product A","description":"cleaning","productParent":null,"images":null},"images":null}]

HTTP GET = localhost:8080/api/v1/product/relationship/{idProduct} : Find specific product with relationship
Return: 
{"id": 2,"name": "Product B", "description": "automotive",
 "productParent": {"id":1,"name":"Product A","description":"cleaning","productParent":null,"images":null},"images":null}

HTTP GET = localhost:8080/api/v1/product/{idProduct}/image : Find all images for specific product
Return: Retorno: [{"id":"1", "type":"jpeg"},{"id":"2", "type":"gif"}]

HTTP GET = localhost:8080/api/v1/product/{idProduct}/parent : Find all child products
Return: [{"id":2,"name":"child 1","description":"child 1","productParent":null,"images":null},{"id":3,"name":"child 2","description":"child 2","productParent":null,"images":null},{"id":4,"name":"child 3","description":"child 1","productParent":null,"images":null}]

HTTP POST = localhost:8080/api/v1/product/{idProduct}/image : Create images for specific product
Input: [{"type": "jpeg"}, {"type": "gif"}, {"type": "ico"}]
Return: Images saved with success

HTTP POST = localhost:8080/api/v1/product/{idProduct}/parent : Create a childs for specific product
Input: [{"name":"child 1", "description" : "child 1"}, {"name":"child 2", "description" : "child 2"}, {"name":"child 3", "description" : "child 3"}, {"name":"child 4", "description" : "child 4"}]
Return: Products parents saved with success


